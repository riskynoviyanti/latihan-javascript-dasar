// Membuat Object 
// Object Literal
var mhs = {
    nama : "Risky Noviyanti",
    nrp : '024567445',
    email : 'riskynoviyanti57@gmail.com',
    jurusan : 'Rekayasa Perangkat Lunak'
}

var mhs2 = {
    nama : "Syifa",
    nrp : '00067445',
    email : 'alya@gmailcom',
    jurusan : 'Tata Busana'
}

//Function Declaration
function buatObjectMahasiswa(nama, nrp, email, jurusan) {
    var mhs = {};
    mhs.nama = nama;
    mhs.nrp = nrp;
    mhs.email = email;
    mhs.jurusan = jurusan;
    return mhs;
}

var mhs3 = buatObjectMahasiswa('Novita', '00345221', 'novita@gmail.com', 'Teknik Jaringan Komunikasi');


// Constructor
function Mahasiswa(nama, nrp, email, jurusan) {
    // var this = {};
    this.nama = nama;
    this.nrp = nrp;
    this.email = email;
    this.jurusan = jurusan;
    // return this
}

var mhs4 = new Mahasiswa('Reza', '006543798', 'Reza@gmail.com', 'Teknik Kendaraan Ringan Otomotif');