// Manipulasi Array

// 1.Menambah isi array
// var arr = [];
// arr[0] = "Risky";
// arr[1] = "Noviyanti";
// arr[2] = "novi";
// arr[6] = "viya";

//console.log(arr);

// 2. Menghapus isi array
// var arr = ["Risky", "Noviyanti", "Novi"];
// arr[1] = undefined;
// console.log(arr);


// 3. Menampilkan isi array
// var arr = ["Risky", "Noviyanti", "Novi"];

// for( var i = 0; i < arr.length; i++ ) {
//    console.log('Mahasiswa ke-' + (i+1) + ' : ' + arr[i]);
// }

// Method pada array
var arr = ['Risky', 'Noviyanti', 'Novi'];
// 1.join
// console.log(arr.join(' - '));

// 2. push & pop
// arr.push('Risky', 'Noviyanti');
// arr.pop();
// arr.pop();
console.log(arr.join(' - '));

// 3. unshift & shift
// arr.unshift('Risky');
// arr.shift();
// console.log(arr.join(' - '));

var arr = ['Risky', 'Noviyanti', 'Novi'];
// 4.splice
// splice(indexAwal, mauDihapusBerapa, elemenBaru1, elemenBaru2, ...)
// arr.splice(1, 2, 'Risky', 'Noviyanti');
// console.log(arr.join(' - '));

// 5. slice
// slice(awal,akhir);
// var arr = ['Risky', 'Noviyanti', 'Novi', 'Viya', 'Noviya'];
// var arr2 = arr.slice(1,4);
// console.log(arr2.join(' - '));
// console.log(arr2.join(' - '));

// 6. foreach
var angka = [1,2,3,4,5,6,7,8];
var nama = ['Risky', 'Noviyanti', 'Novi'];
// for( var i = 0; i < angka.length; i++ ) {
//    console.log(angka[i]);
// }

// angka.forEach(function(e){
//    console.log(e);
// });
// nama.forEach(function(e, i) {
    console.log('Mahasiswa ke-' + (i+1) + ' adalah : ' + e);
// })

// 7. map
// var angka = [1,2,5,3,6,8,4];
// var angka2 = angka.map(function(e){
//    return e * 2;
// });
console.log(angka2.join(' - '));


// 8. sort
// var angka = [1,2,10,5,20,3,6,8,4];
// angka.sort(function(a,b) {
//    return a-b;
// });
// console.log(angka.join(' - '));


// 9. filter
var angka = [1,2,10,5,20,3,6,8,4];
var angka2 = angka.filter(function(x) {
    return x > 5;
});
console.log(angka2);